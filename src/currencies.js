import { readable } from 'svelte/store';

export let currencies = readable(
	[
		{
			countryiso: "AF",
			currencies: [
				{
					currencyiso: "AFN"
				}
			]
		},
		{
			countryiso: "XP",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AL",
			currencies: [
				{
					currencyiso: "EUR"
				},
				{
					currencyiso: "ALL"
				}
			]
		},
		{
			countryiso: "DZ",
			currencies: [
				{
					currencyiso: "DZD"
				}
			]
		},
		{
			countryiso: "AS",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AD",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "AO",
			currencies: [
				{
					currencyiso: "AOA"
				}
			]
		},
		{
			countryiso: "AI",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "AG",
			currencies: [
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "AR",
			currencies: [
				{
					currencyiso: "ARS"
				}
			]
		},
		{
			countryiso: "AW",
			currencies: [
				{
					currencyiso: "AWG"
				}
			]
		},
		{
			countryiso: "AU",
			currencies: [
				{
					currencyiso: "AUD"
				}
			]
		},
		{
			countryiso: "AT",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "AZ",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "AZN"
				}
			]
		},
		{
			countryiso: "BS",
			currencies: [
				{
					currencyiso: "BSD"
				}
			]
		},
		{
			countryiso: "BH",
			currencies: [
				{
					currencyiso: "BHD"
				}
			]
		},
		{
			countryiso: "XB",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BD",
			currencies: [
				{
					currencyiso: "BDT"
				}
			]
		},
		{
			countryiso: "BB",
			currencies: [
				{
					currencyiso: "BBD"
				}
			]
		},
		{
			countryiso: "BE",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QQ",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BZ",
			currencies: [
				{
					currencyiso: "BZD"
				}
			]
		},
		{
			countryiso: "BJ",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "BM",
			currencies: [
				{
					currencyiso: "BMD"
				}
			]
		},
		{
			countryiso: "BT",
			currencies: [
				{
					currencyiso: "BTN"
				}
			]
		},
		{
			countryiso: "BO",
			currencies: [
				{
					currencyiso: "BOB"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BA",
			currencies: [
				{
					currencyiso: "BAM"
				}
			]
		},
		{
			countryiso: "BW",
			currencies: [
				{
					currencyiso: "BWP"
				}
			]
		},
		{
			countryiso: "BR",
			currencies: [
				{
					currencyiso: "BRL"
				}
			]
		},
		{
			countryiso: "VG",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BN",
			currencies: [
				{
					currencyiso: "BND"
				}
			]
		},
		{
			countryiso: "BG",
			currencies: [
				{
					currencyiso: "EUR"
				},
				{
					currencyiso: "BGN"
				}
			]
		},
		{
			countryiso: "BF",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "BI",
			currencies: [
				{
					currencyiso: "BIF"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "KH",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CM",
			currencies: [
				{
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "CA",
			currencies: [
				{
					currencyiso: "CAD"
				}
			]
		},
		{
			countryiso: "CV",
			currencies: [
				{
					currencyiso: "CVE"
				}
			]
		},
		{
			countryiso: "KY",
			currencies: [
				{
					currencyiso: "KYD"
				}
			]
		},
		{
			countryiso: "CF",
			currencies: [
				{
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "TD",
			currencies: [
				{
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "CL",
			currencies: [
				{
					currencyiso: "CLP"
				}
			]
		},
		{
			countryiso: "CN",
			currencies: [
				{
					currencyiso: "CNY"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CO",
			currencies: [
				{
					currencyiso: "COP"
				}
			]
		},
		{
			countryiso: "KM",
			currencies: [
				{
					currencyiso: "KMF"
				}
			]
		},
		{
			countryiso: "CD",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CG",
			currencies: [
				{
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "CK",
			currencies: [
				{
					currencyiso: "NZD"
				}
			]
		},
		{
			countryiso: "CR",
			currencies: [
				{
					currencyiso: "CRC"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HR",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "CU",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "QS",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AN",
			currencies: [
				{
					currencyiso: "ANG"
				}
			]
		},
		{
			countryiso: "CY",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "C2",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CZ",
			currencies: [
				{
					currencyiso: "CZK"
				}
			]
		},
		{
			countryiso: "DK",
			currencies: [
				{
					currencyiso: "DKK"
				}
			]
		},
		{
			countryiso: "DJ",
			currencies: [
				{
					currencyiso: "DJF"
				}
			]
		},
		{
			countryiso: "QV",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "DM",
			currencies: [
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "DO",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "DOP"
				}
			]
		},
		{
			countryiso: "TP",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "EC",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "EG",
			currencies: [
				{
					currencyiso: "EGP"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "SV",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GQ",
			currencies: [
				{
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "ER",
			currencies: [
				{
					currencyiso: "ERN"
				}
			]
		},
		{
			countryiso: "EE",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "ET",
			currencies: [
				{
					currencyiso: "ETB"
				}
			]
		},
		{
			countryiso: "FK",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "FKP"
				}
			]
		},
		{
			countryiso: "FJ",
			currencies: [
				{
					currencyiso: "FJD"
				}
			]
		},
		{
			countryiso: "FI",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "FR",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "GF",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "GA",
			currencies: [
				{
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "GM",
			currencies: [
				{
					currencyiso: "GMD"
				}
			]
		},
		{
			countryiso: "GE",
			currencies: [
				{
					currencyiso: "GEL"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "DE",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QO",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GH",
			currencies: [
				{
					currencyiso: "GHS"
				}
			]
		},
		{
			countryiso: "GI",
			currencies: [
				{
					currencyiso: "GBP"
				}
			]
		},
		{
			countryiso: "GR",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QZ",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GD",
			currencies: [
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "GP",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "GU",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "XY",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GT",
			currencies: [
				{
					currencyiso: "GTQ"
				}
			]
		},
		{
			countryiso: "GN",
			currencies: [
				{
					currencyiso: "GNF"
				}
			]
		},
		{
			countryiso: "GW",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "GY",
			currencies: [
				{
					currencyiso: "GYD"
				}
			]
		},
		{
			countryiso: "HT",
			currencies: [
				{
					currencyiso: "HTG"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HN",
			currencies: [
				{
					currencyiso: "HNL"
				}
			]
		},
		{
			countryiso: "QR",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HK",
			currencies: [
				{
					currencyiso: "HKD"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HU",
			currencies: [
				{
					currencyiso: "HUF"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IS",
			currencies: [
				{
					currencyiso: "ISK"
				},
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "XM",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IN",
			currencies: [
				{
					currencyiso: "INR"
				}
			]
		},
		{
			countryiso: "ID",
			currencies: [
				{
					currencyiso: "IDR"
				}
			]
		},
		{
			countryiso: "IQ",
			currencies: [
				{
					currencyiso: "IQD"
				}
			]
		},
		{
			countryiso: "QX",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IE",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "IL",
			currencies: [
				{
					currencyiso: "ILS"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IT",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QP",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CI",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "JM",
			currencies: [
				{
					currencyiso: "JMD"
				}
			]
		},
		{
			countryiso: "JP",
			currencies: [
				{
					currencyiso: "JPY"
				}
			]
		},
		{
			countryiso: "QM",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "JO",
			currencies: [
				{
					currencyiso: "JOD"
				}
			]
		},
		{
			countryiso: "KZ",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "KZT"
				}
			]
		},
		{
			countryiso: "KE",
			currencies: [
				{
					currencyiso: "KES"
				}
			]
		},
		{
			countryiso: "KR",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "KRW"
				}
			]
		},
		{
			countryiso: "QN",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "K1",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "XF",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "KW",
			currencies: [
				{
					currencyiso: "KWD"
				}
			]
		},
		{
			countryiso: "QU",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "KG",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AA",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LA",
			currencies: [
				{
					currencyiso: "LAK"
				}
			]
		},
		{
			countryiso: "LV",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "LB",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LS",
			currencies: [
				{
					currencyiso: "LSL"
				}
			]
		},
		{
			countryiso: "LR",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LI",
			currencies: [
				{
					currencyiso: "CHF"
				}
			]
		},
		{
			countryiso: "LT",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "LU",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MO",
			currencies: [
				{
					currencyiso: "MOP"
				}
			]
		},
		{
			countryiso: "MK",
			currencies: [
				{
					currencyiso: "EUR"
				},
				{
					currencyiso: "MKD"
				}
			]
		},
		{
			countryiso: "MG",
			currencies: [
				{
					currencyiso: "MGA"
				}
			]
		},
		{
			countryiso: "MW",
			currencies: [
				{
					currencyiso: "MWK"
				}
			]
		},
		{
			countryiso: "MY",
			currencies: [
				{
					currencyiso: "MYR"
				}
			]
		},
		{
			countryiso: "MV",
			currencies: [
				{
					currencyiso: "MVR"
				}
			]
		},
		{
			countryiso: "ML",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "MT",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MH",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "MQ",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MU",
			currencies: [
				{
					currencyiso: "MUR"
				}
			]
		},
		{
			countryiso: "MX",
			currencies: [
				{
					currencyiso: "MXN"
				}
			]
		},
		{
			countryiso: "FM",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "MD",
			currencies: [
				{
					currencyiso: "USD"
				},
				{
					currencyiso: "MDL"
				},
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MN",
			currencies: [
				{
					currencyiso: "MNT"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "ME",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MS",
			currencies: [
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "MA",
			currencies: [
				{
					currencyiso: "MAD"
				}
			]
		},
		{
			countryiso: "MZ",
			currencies: [
				{
					currencyiso: "MZN"
				}
			]
		},
		{
			countryiso: "MM",
			currencies: [
				{
					currencyiso: "MMK"
				}
			]
		},
		{
			countryiso: "NA",
			currencies: [
				{
					currencyiso: "NAD"
				}
			]
		},
		{
			countryiso: "NR",
			currencies: [
				{
					currencyiso: "AUD"
				}
			]
		},
		{
			countryiso: "NP",
			currencies: [
				{
					currencyiso: "NPR"
				}
			]
		},
		{
			countryiso: "NL",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QT",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "NZ",
			currencies: [
				{
					currencyiso: "NZD"
				}
			]
		},
		{
			countryiso: "NI",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "NE",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "NG",
			currencies: [
				{
					currencyiso: "NGN"
				}
			]
		},
		{
			countryiso: "NU",
			currencies: [
				{
					currencyiso: "NZD"
				}
			]
		},
		{
			countryiso: "MP",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "NO",
			currencies: [
				{
					currencyiso: "NOK"
				}
			]
		},
		{
			countryiso: "OM",
			currencies: [
				{
					currencyiso: "OMR"
				}
			]
		},
		{
			countryiso: "PR",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PK",
			currencies: [
				{
					currencyiso: "PKR"
				}
			]
		},
		{
			countryiso: "PW",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PS",
			currencies: [
				{
					currencyiso: "ILS"
				},
				{
					currencyiso: "JOD"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PA",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PG",
			currencies: [
				{
					currencyiso: "PGK"
				}
			]
		},
		{
			countryiso: "PY",
			currencies: [
				{
					currencyiso: "PYG"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PE",
			currencies: [
				{
					currencyiso: "PEN"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PH",
			currencies: [
				{
					currencyiso: "PHP"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PL",
			currencies: [
				{
					currencyiso: "PLN"
				}
			]
		},
		{
			countryiso: "PT",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "XT",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "QA",
			currencies: [
				{
					currencyiso: "QAR"
				}
			]
		},
		{
			countryiso: "QY",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "RO",
			currencies: [
				{
					currencyiso: "EUR"
				},
				{
					currencyiso: "RON"
				}
			]
		},
		{
			countryiso: "XW",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "RW",
			currencies: [
				{
					currencyiso: "RWF"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BL",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "KN",
			currencies: [
				{
					currencyiso: "XCD"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LC",
			currencies: [
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "VC",
			currencies: [
				{
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "XU",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "WS",
			currencies: [
				{
					currencyiso: "WST"
				}
			]
		},
		{
			countryiso: "SA",
			currencies: [
				{
					currencyiso: "SAR"
				}
			]
		},
		{
			countryiso: "SN",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "YU",
			currencies: [
				{
					currencyiso: "EUR"
				},
				{
					currencyiso: "RSD"
				}
			]
		},
		{
			countryiso: "SC",
			currencies: [
				{
					currencyiso: "SCR"
				}
			]
		},
		{
			countryiso: "SL",
			currencies: [
				{
					currencyiso: "SLE"
				}
			]
		},
		{
			countryiso: "SG",
			currencies: [
				{
					currencyiso: "SGD"
				}
			]
		},
		{
			countryiso: "SK",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "SI",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "SB",
			currencies: [
				{
					currencyiso: "SBD"
				}
			]
		},
		{
			countryiso: "ZA",
			currencies: [
				{
					currencyiso: "ZAR"
				}
			]
		},
		{
			countryiso: "ES",
			currencies: [
				{
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "AB",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LK",
			currencies: [
				{
					currencyiso: "LKR"
				}
			]
		},
		{
			countryiso: "S1",
			currencies: [
				{
					currencyiso: "ANG"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "SD",
			currencies: [
				{
					currencyiso: "SDG"
				}
			]
		},
		{
			countryiso: "SR",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "SE",
			currencies: [
				{
					currencyiso: "SEK"
				}
			]
		},
		{
			countryiso: "CH",
			currencies: [
				{
					currencyiso: "CHF"
				}
			]
		},
		{
			countryiso: "TW",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TJ",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TZ",
			currencies: [
				{
					currencyiso: "TZS"
				}
			]
		},
		{
			countryiso: "TH",
			currencies: [
				{
					currencyiso: "THB"
				}
			]
		},
		{
			countryiso: "XV",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TG",
			currencies: [
				{
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "TO",
			currencies: [
				{
					currencyiso: "TOP"
				}
			]
		},
		{
			countryiso: "TT",
			currencies: [
				{
					currencyiso: "TTD"
				}
			]
		},
		{
			countryiso: "TN",
			currencies: [
				{
					currencyiso: "TND"
				}
			]
		},
		{
			countryiso: "TR",
			currencies: [
				{
					currencyiso: "TRY"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "XN",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TM",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TC",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TV",
			currencies: [
				{
					currencyiso: "AUD"
				}
			]
		},
		{
			countryiso: "XE",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "UG",
			currencies: [
				{
					currencyiso: "UGX"
				}
			]
		},
		{
			countryiso: "UA",
			currencies: [
				{
					currencyiso: "UAH"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AE",
			currencies: [
				{
					currencyiso: "AED"
				}
			]
		},
		{
			countryiso: "GB",
			currencies: [
				{
					currencyiso: "GBP"
				}
			]
		},
		{
			countryiso: "QW",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "US",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "UY",
			currencies: [
				{
					currencyiso: "UYU"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "UZ",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "VU",
			currencies: [
				{
					currencyiso: "VUV"
				}
			]
		},
		{
			countryiso: "VE",
			currencies: [
				{
					currencyiso: "VEF"
				}
			]
		},
		{
			countryiso: "VN",
			currencies: [
				{
					currencyiso: "VND"
				},
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "YE",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "ZM",
			currencies: [
				{
					currencyiso: "ZMW"
				}
			]
		},
		{
			countryiso: "ZW",
			currencies: [
				{
					currencyiso: "USD"
				}
			]
		}
	]
);

export default currencies;