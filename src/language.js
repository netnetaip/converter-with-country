import { readable } from 'svelte/store';

export let defaultLang = readable({
	EN: {
		sendto: "Send to ",
		sendlabel: "You send",
		receivelabel: "Recipient gets",
		searchterm: "Type a country",
		tablefee: "Transfer fee",
		tablerate: "Exchange rate",
		tablesend: "Payment method",
		tablereceive: "Receive method",
		tabletotal: "Total to pay",
		cta: "Send money",
		claimratefee: "Exchange rates and fees shown are estimates and may vary by payment and payout methods or other factors and are subject to change.",
		CC: "Credit/Debit Card",
		GP: "Google Pay",
		AP: "Apple Pay",
		EB: "Bank transfer",
		PA: "Plaid",
		TR: "Trustly",
		SF: "Sofort",
		BN: "Bancontact",
		"000": "Cash",
		"200": "Direct to Card",
		"300": "Next Day/Delayed Services",
		"500": "Bank account",
		"501": "Western Union Digital Bank Account",
		"800": "Mobile Wallet",
	}
})
export default defaultLang;