import { writable } from 'svelte/store';

// Sender
export const sendIso = writable("GB");
export const sendCurnc = writable("GBP");
export const sendLang = writable("EN");

// Default Amount
export const sendAmount = writable(100);

// Receiver
export const receiveIso = writable("IN");

// Promo
export const payinPromo = writable("CC");
export const payoutPromo = writable("500");

// Do not change
export const promise = writable(null);
export const receiveAmount = writable(null);
export const receiveCurnc = writable(null);
export const hostName = writable("https://www.westernunion.com");
export const isLoading = writable(false);
export const serviceGroup = writable([]);
export const payinSelected = writable();
export const payoutSelected = writable();
export const uuidv4 = writable(null);
export const serviceSelected = writable([]);
export const sendCountryName = writable();
export const currencyArray = writable([]);
export const payins = writable(false);
export const payouts = writable(false);